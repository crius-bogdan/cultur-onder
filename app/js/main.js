(function(doc) {
    const stickyFormWrap = doc.querySelector('.scl-form-wrap'),
          stikyForm = doc.querySelector('.scl-form'),
          constactSection = doc.querySelector('.contact-sect');

    function smoothScroll(e) {
        e.preventDefault();
        let target = e.target;
        if (target.hash !== '') {
            const hash = target.hash.replace("#", "")
            const link = doc.getElementsByName(hash);
            const position = getCoords(link[0])
            let top = window.scrollY,
                num = 35;

            if (window.innerWidth < 768) {
                num = 75
            }

            let smooth = setInterval(() => {
                let leftover = position - top,
                    topOver = top - position;
                // console.log();

                if (top === position) {
                    clearInterval(smooth)
                } else if (position > top && leftover < num && position !== 0) {
                    top += leftover
                    window.scrollTo(0, top)
                } else if (position > (top - num) && position !== 0 && leftover > 0) {
                    top += num
                    window.scrollTo(0, top)
                } else if (position < top && topOver > num) {
                    top -= num;

                    if (topOver < num) {
                        // top -= topOver;
                        window.scrollTo(0, position)
                    } else {
                        window.scrollTo(0, top)
                    }
                }
            }, 6)
        }
    }
    function getCoords(elem) {
        let box = elem.getBoundingClientRect();

        return box.top + pageYOffset;
    }
    function setCursorPosition(pos, elem) {
        elem.focus();
        if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
        else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd("character", pos);
            range.moveStart("character", pos);
            range.select()
        }
    }
    function mask() {
        var matrix = this.defaultValue,
            i = 0,
            def = matrix.replace(/\D/g, ""),
            val = this.value.replace(/\D/g, "");
        def.length >= val.length && (val = def);
        matrix = matrix.replace(/[X\d]/g, function (a) {
            return val.charAt(i++) || "X"
        });
        this.value = matrix;
        i = matrix.lastIndexOf(val.substr(-1));
        i < matrix.length && matrix != this.defaultValue ? i++ : i = matrix.indexOf("X");
        setCursorPosition(i, this)
    }
    
    doc.addEventListener('scroll', () => {
        let scrollPosition = window.scrollY;
        if (window.innerWidth > 1000) {
            if ((getCoords(stickyFormWrap) - 110) < scrollPosition) {
                stikyForm.classList.add('sticky')
                // stikyForm.style.top = `${scrollPosition}px`;
            } 
            if ((getCoords(constactSection) - 1100) < scrollPosition) {
                stikyForm.classList.add('op')
            } else {
                stikyForm.classList.remove('op')
            }
            if ((getCoords(stickyFormWrap) - 110) > scrollPosition) {
                stikyForm.classList.remove('sticky')
            }
        }
        if (window.innerWidth < 1000) {
            let secondSect = doc.querySelector('.verdeling-sect'),
                stikyBar = doc.querySelector('.sticky-bar');
            if ((getCoords(secondSect) + 200) < scrollPosition) {
                stikyBar.classList.add('visible')
            } else {
                stikyBar.classList.remove('visible')
            }
        }
    });
    
    doc.querySelector('.up-btn').addEventListener('click', smoothScroll)
    doc.querySelector('.bar-link').addEventListener('click', smoothScroll)
    const inputPhone = doc.querySelectorAll('input[name="phone"]');


    for(let input of inputPhone) {
        input.addEventListener('click', function () {
            this.setAttribute('value', '+31-XXX-XXXX-XX');
            setCursorPosition(4, this);
        });
    
        input.addEventListener('keyup', mask, false);
    }


})(document)